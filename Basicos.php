<?php
session_start ();
require_once ("{$_SESSION['root']}/librerias.php");
require_once ("{$_SESSION['root']}/model/WebModel.php");

$wmodel 			= new WebModel ();
$wmodel->Head();
require_once ("CtrlBasicos.php");

$municipio      ->load ( "codigo = ? and id_dep = ?", array ($paciente->cod_muni,$paciente->cod_dep));
$departamento   ->load ( "codigo = ? ", array ($paciente->cod_dep));
$barrios        ->load ( "codigo = ? ", array ($paciente->barrio));
$eps            ->load ( "codigo = ? ", array ($paciente->cod_eps));
$arp            ->load ( "codigo = ? ", array ($paciente->codigo_arp));
$pension        ->load ( "codigo = ? ", array ($paciente->codigo_pensiones));
$prepagada      ->load ( "codigo = ? ", array ($paciente->codigo_prepagada));
$caja           ->load ( "codigo = ? ", array ($paciente->cod_caja));
$comisionados   ->load ( "codigo = ? ", array ($paciente->comisionado));
$paciente       ->fecha_naci = str_replace ( "/", "-", $paciente->fecha_naci );

$fechaNacimiento = explode ( "-", $paciente->fecha_naci );

// $xnho = $fechaNacimiento [0];
// $xmes = $fechaNacimiento [1];
// $xdia = $fechaNacimiento [2];

$xnho = valueOf($xnho, $fechaNacimiento [0]);
$xmes = valueOf($xmes, $fechaNacimiento [1]);
$xdia = valueOf($xdia, $fechaNacimiento [2]);

$foto = file_exists ( $_SESSION ['root'] . "/archivos/Pacientes/Fotos/" . $paciente->num_id . ".gif" ) == true ? $_SESSION ['path'] . "/archivos/Pacientes/Fotos/" . $paciente->num_id . ".gif" : $_SESSION ['path'] . "/imagenes/face.png";
clearstatcache();
function valueOf($request, $value, $default = ''){
	return !(empty($request) && $request != '0') ? $request : ( empty($value) && $value != '0' ? $default : $value );
}
?>
<script>
	console.log("<?php echo 	$_SESSION['nombre_sede']." ".$_SESSION['codigo_sede']; ?>");
	function inicial(){
		<?php echo $mensaje != '' ? 'new PNotify({ title: \'Informe Transaccion\', type: \'success\', hide: true, text: \'' . $mensaje . '\', icon: \''.$icono.'\', delay: 700});' : ""?> 
		<?php echo $mensajeError != '' ? 'new PNotify({ title: \'Informe Transaccion\', type: \'error\', hide: true, text: \'' . $mensajeError . '\', icon: \''.$icono.'\', delay: 700});' : ""?> 
	}
	setTimeout(inicial, 1000);
</script>
<script src="<?php echo $_SESSION['path'] ?>/comun/js/vue.js"></script>
<script src="<?php echo $_SESSION['path'] ?>/comun/js/axios.min.js"></script>
<script src="<?php echo $_SESSION['path'] ?>/comun/js/lodash.min.js"></script>
<style>
	.selectAsistencial{
		width: 39%;
	}
	.selectDatosBasicos{
		width: 20%;
	}
	.selectInfoAcademica{
		width: 30%;
	}
	button#verificador{
		width: 13%;
		background-color: #516f99;
		border-color:#516f99;
		color:#ffffff;
		margin-left: 2%;
	}
</style>
<script>
	Vue.config.devtools = true;
</script>

<body onLoad='javascript:inicial()'>
	<form method="post" name="formulario" id="formulario"
		enctype="multipart/form-data">
		<table width="100%" border="0" cellpadding="1" cellspacing="1"
			class="tablas">
			<tr>
				<td width="52%" height="65" valign="top" scope="col">
					<table width="100%" height="61" border="0" cellpadding="1"
						cellspacing="1" class="LSBOTH">
						<tr>
							<td width="6%" height="57" align="center"><a
								href="ListaPaciente.php"> <img
									src="<?php echo $_SESSION['path']; ?>/imagenes/basico2.gif"
									alt="Datos Basicos" border="0"
									title="Datos Basicos" />
							</a></td>
							<td width="6%" align="center" class="mediumhead"><a
								href="javascript:abrirOpcion('CtrlAdmitir.php?ident=<?php echo $paciente->num_id; ?>&amp;tipo=<?php echo $paciente->tipo_id; ?>', 'formulario')">
									<img
									src="<?php echo $_SESSION['path']; ?>/imagenes/admitir2.gif"
									alt="Admitir Paciente" border="0"
									title="Admitir" /><br />
							</a></td>
							<td width="6%" align="center" class="mediumhead"><span
								class="naranjadoII"><a
									href="javascript:abrirOpcion('CtrlFacturar.php', 'formulario')"><img
										src="<?php echo $_SESSION['path']; ?>/imagenes/facturar.gif"
										alt="Facturar" border="0"
										title="Facturar" /></a></span></td>
							<td width="6%" align="center" class="mediumhead"><a
								href="javascript:abrirOpcion('../Caja/CajaCopago.php', 'formulario')"><img
									src="<?php echo $_SESSION['path']; ?>/imagenes/copagos.gif"
									alt="Caja de Copago" border="0"
									title="Pagos" /></a></td>
							<td width="76%" align="center" class="mediumhead">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td height="655" valign="top" scope="col">&nbsp;
					<table width="629" border="0" align="center" cellpadding="0"
						cellspacing="2" class="tablas">
						<tr>
							<td width="80%" height="19"
								background="<?php echo $_SESSION['path']; ?>/imagenes/bgtop.gif"
								class="letraDisplayIV">&nbsp;&nbsp;<span class="letraCaptionNegrita">Datos del Paciente</span>
    					</td>
						</tr>
						<tr>
							<td height="628" valign="top">
								<table width="100%" border="0" align="center" cellpadding="2"
									cellspacing="2" class="tablas">
									<tr>
										<td colspan="3" class="nomGrupo">Informaci&oacute;n Basica</td>
									</tr>
									<tr>
										<td width="169" align="left" class="letraDisplay"><span
											class="letraRed">*</span>&nbsp;Tipo Identificaci&oacute;n </td>
										<td colspan="2"><select style="width: 360px;"   name="tipo_id"
											id="tipo_id"
											class="letraDisplayInput"
											title="Tipo de Identificaci&oacute;n"
											v-on:change="checkTipoId"
											onchange="document.formulario.ident.focus(); get('codigo_tipo_identificacion').value = getValue('tipo_id')">
												<option>[SELECCIONE]</option>
                					<?php $model->elementos('GRL', 'IDNT', 5, valueOf($tipo_id, $paciente->tipo_id, 'CC')); ?>
            						</select> <input name="autoid" type="hidden" id="autoid"
											value="<?php echo valueOf($autoid, $paciente->autoid, '') ?>" />
											<input name="nro_historia" type="hidden" id="nro_historia"
											value="<?php echo valueOf($nro_historia, $paciente->nro_historia, '') ?>" />
										</td>
									</tr>
									<tr>
										<td align="left" class="letraDisplay"><span class="letraRed">*</span>&nbsp;N&uacute;mero de Identificaci&oacute;n</td>
										<td colspan="2"><input name="num_id" type="text"
											class="letraDisplayInput" id="num_id"
											title="Numero de identificacion" onFocus="select()"
											onkeypress="return validFormat(this,event,'Int'+getValue('tipo_id'),'')"
											onkeydown="return revisar(this, 'formulario', event, 'Basicos.php?operacion=buscar_paciente');"
											size="45"
											value="<?php echo valueOf($num_id, $paciente->num_id, '') ?>" v-on:keyup="checkNumId"/>
											<input name="codigo_tipo_identificacion" type="hidden"
											id="codigo_tipo_identificacion"
											value='<?php echo trim($paciente->tipo_id); ?>' />

											<button name="varificador" type="button" v-on:click="verificarDerechos" width="14%"
												class="letraCaptionNegrita"
												id="verificador"
												/> Verificar </button>

											</td>
									</tr>
									<tr>
										<td align="left" class="letraDisplay">&nbsp;</td>
										<td colspan="2">&nbsp;</td>
									</tr>
									<?php if($_SESSION['idlanguage']==3) {?>
									<tr>									
										<td align="left" class="letraDisplay"> &nbsp; N&uacute;mero Seguridad Social </td>
										<td colspan="2"><input name="num_seguridad_social" type="text"
											class="letraDisplayInput" id="num_seguridad_social"
											title="Numero de Seguridad social" 
											size="55"
											value="<?php echo valueOf($num_seguridad_social, $paciente->num_seguridad_social, '') ?>" />
											</td>
									</tr>
									<?php } ?>

									<tr>
										<td align="left" class="letraDisplay"><span class="letraRed">*</span>&nbsp;Primer
											Apellido</td>
										<td width="271"><input name="primer_ape" type="text"
											class="letraDisplayInput" id="primer_ape"
											title="Primer Apellido"
											onkeypress="return validFormat(this,event,'Mayus')"
											v-model="primer_ape"
											value="<?php echo valueOf($primer_ape, $paciente->primer_ape, '') ?>" size="55" /></td>
										<!-- <td width="175" rowspan="4" align="left" valign="middle"><img
											src="<?php echo $foto; ?>" name="foto_paciente" width="96"
											height="105" border="0" id="foto_paciente" /></td> -->
									</tr>
									<tr>
										<td align="left" class="letraDisplay"><samp class="letraRed">*</samp>&nbsp;Segundo
											Apellido</td>
										<td><input name="segundo_ape" type="text"
											class="letraDisplayInput" id="segundo_ape"
											title="Segundo Apellido"
											v-model="segundo_ape"
											onkeypress="return validFormat(this,event,'Mayus')"
											value="<?php echo valueOf($segundo_ape, $paciente->segundo_ape, '') ?>" size="55" /></td>
									</tr>
									<tr>
										<td align="left" class="letraDisplay"><span class="letraRed">*</span>&nbsp;Primer
											Nombre</td>
										<td><input name="primer_nom" type="text"
											class="letraDisplayInput" id="primer_nom"
											title="Primer Nombre"
											onkeypress="return validFormat(this,event,'Mayus')"
											v-model="primer_nom"
											value="<?php echo valueOf($primer_nom, $paciente->primer_nom, '') ?>" size="55" /></td>
									</tr>
									<tr>
										<td height="26" align="left" class="letraDisplay" style="padding-left: 12px;">Segundo Nombre</td>
										<td><input name="segundo_nom" type="text"
											class="letraDisplayInput" id="segundo_nom"
											title="Segundo nombre"
											v-model="segundo_nom"
											value="<?php echo valueOf($segundo_nom, $paciente->segundo_nom, '') ?>"
											size="55" /></td>
									</tr>
									<!-- <tr>
										<td align="left" class="letraDisplay">Cargar Foto</td>
										<td colspan="2"><input name="foto" type="file"
											class="letraDisplayInput" id="foto" size="36" /></td>
									</tr> -->
									<tr>
										<td align="left" class="letraDisplay">&nbsp;</td>
										<td colspan="2">&nbsp;</td>
									</tr>
									<tr>
										<td align="left" class="letraDisplay"><span class="letraRed">*</span>&nbsp;Genero</td>
										<td colspan="2"><select style="width: 360px;" name="sexo" id="sexo"
											class="letraDisplayInput selectDatosBasicos" title="Sexo" v-model="sexo">>
												<option value="">[Seleccione]</option>
												<option value="F" <?php echo valueOf($sexo, $paciente->sexo, '') == 'F' ? "selected" : ""; ?>>Femenino</option>
												<option value="M" <?php echo valueOf($sexo, $paciente->sexo, '') == 'M' ? "selected" : ""; ?>>Masculino</option>
										</select></td>
									</tr>
									<tr>
										<td align="left" class="letraDisplay"><span class="letraRed">*</span>&nbsp;Fecha
											Nacimiento <input name="fecha_naci" type="hidden"
											class="letraDisplayInput" id="fecha_naci"
											title="Fecha de Nacimiento" onFocus="select()"
											onkeypress="return validFormat(this,event,'Date','')"
											value="<?php echo convertirFecha(valueOf($fecha_naci, $paciente->fecha_naci, '')); ?>"
											size="40" maxlength="10" /></td>
										<td colspan="2" class="letraDisplay"><select name="xnho"
											class="letraDisplayInput" id="xanho"  v-model="annio" v-on:change="checkAnnio">
                <?php

$inicio = date ( "Y" ) - 120;
																for($i = 1900; $i <= date ( "Y" ); $i ++) {
																	?>
                    <option value="<?php echo $i; ?>"
										<?php echo valueOf($xnho, '') == $i ? "selected" : "" ?>><?php echo $i; ?></option>
                <?php } ?>
            </select> / <select name="xmes" class="letraDisplayInput"
											id="xmes" v-model="mes"  v-on:change="checkMes">
                <?php

for($i = 1; $i <= 12; $i ++) {
	$texto = $i < 10 ? "0$i" : $i;?>
                              <option value="<?php echo $texto; ?>"
													<?php echo valueOf($xmes,'') == $i ? "selected" : "" ?>><?php echo $texto; ?></option>
                <?php } ?>
            </select> / <select name="xdia" class="letraDisplayInput" 
											id="xdia" v-model="dia" v-on:change="checkDia">
                <?php
for($i = 1; $i <= 31; $i ++) {
	$texto = $i < 10 ? "0$i" : $i;?>
                      <option value="<?php echo $texto; ?>"
													<?php echo valueOf($xdia,'') == $i ? "selected" : "" ?>><?php echo $texto; ?></option>
                <?php } ?>
            </select> (YYYY-MM-DD)</td>
									</tr>

									<!-- SI LA VERSIO ES IMAGENES DIAGNOSCAS NO SE MUESTRA TIPO DE SANGRE -->
									<?php if($_SESSION['idversion'] != '3'){ ?>
										<tr>

											<td align="left" class="letraDisplay" style="padding-left: 12px;" >
											&nbsp;Tipo Sangre </td>
											<td colspan="2"><select  style="width: 360px;" name="tipo_sangre"
												class="letraDisplayInput selectDatosBasicos" id="tipo_sangre"
												title="Tipo Sangre">
													<option value="">[Seleccione]</option>
													<option value="O-"
														<?php echo valueOf($tipo_sangre, $paciente->tipo_sangre, '') == 'O-' ? "selected" : ""; ?>>O-</option>
													<option value="O+"
														<?php echo valueOf($tipo_sangre, $paciente->tipo_sangre, '') == 'O+' ? "selected" : ""; ?>>O+</option>
													<option value="A-"
														<?php echo valueOf($tipo_sangre, $paciente->tipo_sangre, '') == 'A-' ? "selected" : ""; ?>>A-</option>
													<option value="A+"
														<?php echo valueOf($tipo_sangre, $paciente->tipo_sangre, '') == 'A+' ? "selected" : ""; ?>>A+</option>
													<option value="B-"
														<?php echo valueOf($tipo_sangre, $paciente->tipo_sangre, '') == 'B-' ? "selected" : ""; ?>>B-</option>
													<option value="B+"
														<?php echo valueOf($tipo_sangre, $paciente->tipo_sangre, '') == 'B+' ? "selected" : ""; ?>>B+</option>
													<option value="AB-"
														<?php echo valueOf($tipo_sangre, $paciente->tipo_sangre, '') == 'AB-' ? "selected" : ""; ?>>AB-</option>
													<option value="AB+"
														<?php echo valueOf($tipo_sangre, $paciente->tipo_sangre, '') == 'AB+' ? "selected" : ""; ?>>AB+</option>
											</select></td>
											</tr>
									<?php } ?>

									<tr>
										<td align="left" class="letraDisplay"><span class="letraRed">*</span>&nbsp;Estado
											Civil</td>
											<td colspan="2"><select style="width: 360px;" name="estado_civil" id="estado_civil"
											class="letraDisplayInput selectDatosBasicos" title="Estado Civil">
												<option value="">[Seleccione]</option>
                <?php $model->elementos('GRL', 'ECIVIL', 6, valueOf($estado_civil, $paciente->estado_civil, '')); ?>
            </select></td>
									</tr>
									<tr>
										<td align="left" class="letraDisplay"><span class="letraRed">*</span>
											Nro Hijos</td>
											<td colspan="2"><select style="width: 360px; text" name="numero_hijo"
											title="Numero de Hijos" class="letraDisplayInput selectDatosBasicos"
											id="numero_hijo">
												<option value="">[Seleccione]</option>
												<option value="0"
													<?php echo valueOf($numero_hijo, $paciente->numero_hijo, '') == '0' ? "selected" : ""; ?>>0</option>
												<option value="1"
													<?php echo valueOf($numero_hijo, $paciente->numero_hijo, '') == '1' ? "selected" : ""; ?>>1</option>
												<option value="2"
													<?php echo valueOf($numero_hijo, $paciente->numero_hijo, '') == '2' ? "selected" : ""; ?>>2</option>
												<option value="3"
													<?php echo valueOf($numero_hijo, $paciente->numero_hijo, '') == '3' ? "selected" : ""; ?>>3</option>
												<option value="4"
													<?php echo valueOf($numero_hijo, $paciente->numero_hijo, '') == '4' ? "selected" : ""; ?>>4</option>
												<option value="5"
													<?php echo valueOf($numero_hijo, $paciente->numero_hijo, '') == '5' ? "selected" : ""; ?>>5</option>
												<option value="6"
													<?php echo valueOf($numero_hijo, $paciente->numero_hijo, '') == '6' ? "selected" : ""; ?>>6</option>
												<option value="7"
													<?php echo valueOf($numero_hijo, $paciente->numero_hijo, '') == '7' ? "selected" : ""; ?>>7</option>
												<option value="8"
													<?php echo valueOf($numero_hijo, $paciente->numero_hijo, '') == '8' ? "selected" : ""; ?>>8</option>
												<option value="9"
													<?php echo valueOf($numero_hijo, $paciente->numero_hijo, '') == '9' ? "selected" : ""; ?>>9</option>
												<option value="10"
													<?php echo valueOf($numero_hijo, $paciente->numero_hijo, '') == '10' ? "selected" : ""; ?>>10</option>
												<option value="Mayor a 10"
													<?php echo valueOf($numero_hijo, $paciente->numero_hijo, '') == 'Mayor a 10' ? "selected" : ""; ?>>Mayor
													a 10</option>

										</select></td>
									</tr>

									<!-- ESTRATO SOLO APARECE CUANDO SE ESCOGE ESPAÑOL DE COLOMBIA Y LA VERSION NO ES CENTRO MÉDICO -->
									<?php if( $_SESSION['language'] == 'es_co' && $_SESSION['idversion'] != '2' ){ ?>
									<tr>
										<td align="left" class="letraDisplay"><span class="letraRed">*</span>
											Estrato</td>
											<td colspan="2"><select align="left" style="width: 360px; " name="estrato"
											title="Estrato Socio Economico" id="estrato"
											class="letraDisplayInput selectDatosBasicos">
												<option value="">[Seleccione]</option>
												<option value="1"
													<?php echo valueOf($estrato, $paciente->estrato, '') == '1' ? "selected" : ""; ?>>1</option>
												<option value="2"
													<?php echo valueOf($estrato, $paciente->estrato, '') == '2' ? "selected" : ""; ?>>2</option>
												<option value="3"
													<?php echo valueOf($estrato, $paciente->estrato, '') == '3' ? "selected" : ""; ?>>3</option>
												<option value="4"
													<?php echo valueOf($estrato, $paciente->estrato, '') == '4' ? "selected" : ""; ?>>4</option>
												<option value="5"
													<?php echo valueOf($estrato, $paciente->estrato, '') == '5' ? "selected" : ""; ?>>5</option>
												<option value="6"
													<?php echo valueOf($estrato, $paciente->estrato, '') == '6' ? "selected" : ""; ?>>6</option>
										</select></td>
									</tr>
									<?php } ?>

									<!-- RAZA SOLO APARECE CUANDO SE ESCOGE ESPAÑOL DE COLOMBIA Y LA VERSION ES DIFERENTE A LA DE IMAGENES DIAGNOSTICAS-->
									<?php if( $_SESSION['language'] == 'es_co' && $_SESSION['idversion'] != '3'){ ?>
									<tr>
										<td align="left" class="letraDisplay"><span class="letraRed">*</span>
											Etnia</td>
											<td colspan="2"><select style="width: 360px;" name="raza" title="Raza" id="raza"
											class="letraDisplayInput selectDatosBasicos">
												<option value="">[Seleccione]</option>
												<option value="Mestizos"
													<?php echo valueOf($raza, $paciente->raza, '') == 'Mestizos' ? "selected" : ""; ?>>Mestizos
												</option>
												<option value="Blancos"
													<?php echo valueOf($raza, $paciente->raza, '') == 'Blancos' ? "selected" : ""; ?>>Blancos
												</option>
												<option value="Indios"
													<?php echo valueOf($raza, $paciente->raza, '') == 'Indios' ? "selected" : ""; ?>>Indios</option>
												<option value="Mulatos"
													<?php echo valueOf($raza, $paciente->raza, '') == 'Mulatos' ? "selected" : ""; ?>>Mulatos
												</option>
												<option value="Cobriza"
													<?php echo valueOf($raza, $paciente->raza, '') == 'Cobriza' ? "selected" : ""; ?>>Cobriza
												</option>
												<option value="Negros"
													<?php echo valueOf($raza, $paciente->raza, '') == 'Negros' ? "selected" : ""; ?>>Negros
												</option>
												<option value="Cuarterones"
													<?php echo valueOf($raza, $paciente->raza, '') == 'Negros' ? "selected" : ""; ?>>Cuarterones
												</option>
										</select></td>
									</tr>
									<?php } ?>
									
									<!-- LUGAR DE NACIMIENTO APARECE SOLO SI LA VERSION ES DIFERENTE DE CENTRO MÉDICO -->
									<?php if( $_SESSION['idversion'] != '2' ){ ?>
									<tr>
										<td align="left" class="letraDisplay"><span class="letraRed">*</span> Lugar de Nacimiento</td>
										<td colspan="2"><input name="procedencia" type="text"
											onkeypress="return validFormat(this,event,'Mayus')"
											id="procedencia" class="letraDisplayInput"
											value="<?php echo valueOf($procedencia, $paciente->procedencia, '') ?>" size="55"
											title="Procedencia" /></td>
									</tr>
									<?php } ?>

								</table>
								<table width="100%" border="0" align="center" cellpadding="2"
									cellspacing="2" class="tablas">
									<tr>
										<td colspan="2" class="nomGrupo">Informaci&oacute;n Ubicaci&oacute;n</td>
									</tr>
									<tr>
									<td align="left" class="letraDisplay"><span class="letraRed">*</span> Tipo de Vivienda</td>
											<td colspan="2"><select style="width: 360px;" name="vivienda" id="vivienda"
																    class="letraDisplayInput selectDatosBasicos" title="Tipo de Vivienda">
																<option value="">[Seleccione]</option>
																<?php $model->elementos('GRL', 'TVIVI', 6, valueOf($vivienda, $paciente->vivienda, ''));?>
															</select>
										    </td>
									</tr>

									<tr>
										<td width="240" align="left" class="letraDisplay"><span
											class="letraRed">*</span>&nbsp;Direcci&oacute;n</td>
										<td width="446"><input  name="direccion" id="direccion"
											title="Direccion"
											onkeypress="return validFormat(this,event,'Mayus')"
											class="letraDisplayInput"
											value="<?php echo valueOf($direccion, $paciente->direccion, '') ?>"  size="55" /></td>
									</tr>
									<tr>
										<td align="left" class="letraDisplay" style="padding-left: 12px;">Tel&eacute;fono Fijo</td>
										<td><input name="telefono" type="text"
											class="letraDisplayInput" id="telefono" title="Telefono"
											onkeypress="return validFormat(this,event,'Int-')"
											value="<?php echo valueOf($telefono, $paciente->telefono, '') ?>" size="55" /></td>
									</tr>
									<tr>
										<td align="left" class="letraDisplay"><span class="letraRed">*</span>
											Tel&eacute;fono M&oacute;vil</td>
										<td><input name="tel_celular" type="text"
											class="letraDisplayInput" id="tel_celular"
											onkeypress="return validFormat(this,event,'Int')"
											value="<?php echo valueOf($tel_celular, $paciente->tel_celular, '') ?>" size="55"
											title="Telefono Movil" /></td>
									</tr>
									<tr>
										<td align="left" class="letraDisplay"style="padding-left: 12px;">Otro Telefono</td>
										<td><input name="tel_oficina" type="text"
											class="letraDisplayInput" id="tel_oficina"
											onkeypress="return validFormat(this,event,'Mayus')"
											value="<?php echo valueOf($tel_oficina, $paciente->tel_oficina, '') ?>" size="55"
											title="Telefono Oficina" /></td>
									</tr>
									<tr>
										<td align="left" class="letraDisplay"><span class="letraRed">*</span> Email</td>
										<td><input name="email" type="text" class="letraDisplayInput"
											id="email"
											onkeypress="return validFormat(this,event,'Minus')"
											value="<?php echo valueOf($email, $paciente->email, '') ?>" size="55"
											title="Email" /></td>
									</tr>
									<tr>
									<td align="left" class="letraDisplay"style="padding-left: 12px;">Pais</td>
										<td>
											<select  style="width: 360px;" name="nombre_pais" title="Pais" class="input" id="nombre_pais" style="width: 80%;"
												ref="nombre_pais" @change="buscarCiudad($event)">
												<option value="">[SELECCIONE]</option>
												<?php 
													echo $model->crearCombo("paises", "descripcion", "descripcion as des", 
														"idpais IS NOT NULL AND descripcion IS NOT NULL AND idpais='34' ORDER BY des ASC", 
														valueOf($nombre_pais, $paciente->nombre_pais, 'COLOMBIA') );
												?>
											</select>
										</td>
									</tr>
									<tr>
									<td align="left" class="letraDisplay"style="padding-left: 12px;"><?php echo $i18n->translate('ciudad'); ?></td>
										<td>
											<input type="hidden" name="nombre_ciudad" id="nombre_ciudad" ref="nombre_ciudad" 
												value="<?php echo valueOf($nombre_ciudad, '') ?>">
											
												
											
											<?php 
												 $nombrePais = valueOf($nombre_pais, $paciente->nombre_pais, 'COLOMBIA');
												 $codigo_pais = $model->getDato('idpais', 'paises', "descripcion = '{$nombrePais}'");
											 ?>
											<select style="width: 360px;" name="cod_muni" title="Ciudad" class="input" id="cod_muni" ref="cod_muni" style="width: 80%;">
 												<option value="" selected>[SELECCIONE]</option>
												<?php 
													echo $model->crearComboCiudad("sis_muni m,departamentos d","m.codigo","CONCAT(m.nombre,' - (',d.nombre,')')", 
														"m.id_dep=d.codigo AND m.codigo is not null and m.nombre is not null and 
														m.idpais = '". $codigo_pais ."' order by m.nombre asc", 
														valueOf($cod_muni, $paciente->cod_muni, '') ) ;
												?>
											</select>
											
										</td>
									</tr>
									<tr>
										<td align="left" class="letraDisplay"><span class="letraRed">*</span>&nbsp;<?php echo $i18n->translate('barrio'); ?></td>
										<td>
	 										<input name="barrio" type="hidden" class="letraDisplayInput" id="barrio" 
	 											value="<?php echo valueOf($barrio, $paciente->barrio, '') ?>" size="6"/>
                                            <input name="nombre_barrio"	title="Barrio" type="text" class="cajaBuscar" 
                                            		id="nombre_barrio" ref="nombre_barrio"
		                                            @keyup="_.delay( () => filtarBarrios($event), 150)"
		                                            value="<?php echo utf8_decode( $model->getDato('nombres', 'sis_barrios', "codigo = '". valueOf($barrio, $paciente->barrio, '') ."'") ); ?>" size="55" />
											<input type="hidden" name="cod_dep" id="cod_dep" value="<?php echo  !empty($paciente->cod_dep) ? $paciente->cod_dep: $model->getDato('id_dep', 'sis_muni', "codigo = '{$cod_muni}'")  ?>" />
											<div id="capa_barrio" style="visibility: visible; z-index: 0;" />

											<input name="fechacreacion2" type="hidden" id="fechacreacion2" 
											value="<?php echo valueOf($fechacreacion, $paciente->fechacreacion, date('Y/m/d')) ?>"
											maxlength="10" /> 
										</td>
									</tr>
									
								</table>

								<!-- SECCION DE INFORMACION ACADEMICA SOLO SE MUESTRA SI LA VERSION ES DIFERENTE DE IMAGENES DIAGNOSTICAS -->
								<?php if($_SESSION['idversion'] != '3'){ ?>
								<table width="100%" border="0" cellpadding="2" cellspacing="2"
									class="tablas">
									<tr>
										<td colspan="2" class="nomGrupo">Informaci&oacute;n Academica</td>
									</tr>

									<!-- ESCOLARIDAD SOLO APARECE CUANDO LA VERSION ES DIFERENTE DE CENTRO MÉDICO -->
									<?php if( $_SESSION['idversion'] != '2' ){ ?>
									<tr>
										<td width="240" align="left"><span class="letraRed">*</span><span class="letraDisplay">
												Escolaridad</span></td>
										<td width="446">
											<select style="width: 360px;" name="escolaridad" id="escolaridad" class="letraDisplayInput selectInfoAcademica" title="Escolaridad">
												<option value="">[Seleccione]</option>
												<?php echo $model->crearCombo("sismaelm with(nolock)", "valor", "desplegable", "tabla='GRL' AND TIPO='ESC' AND lenguaje = {$_SESSION['idlanguage']} ORDER BY VALOR ", valueOf($escolaridad, $paciente->escolaridad, '')); ?>
			            					</select>
			            				</td>
									</tr>
									<?php } ?>

									<tr>
										<td align="left" class="letraDisplay"><span class="letraRed">*</span> Estado Academico</td>
										<td><select style="width: 360px;" name="estado_academico" id="estado_academico"
											title="Estado Academico" class="letraDisplayInput selectInfoAcademica">
												<option value="">[Seleccione]</option>
												<option value="Completo"
												<?php echo valueOf($estado_academico, $paciente->estado_academico, '') == 'Completo' ? "selected" : "" ?>>
													Completo</option>
												<option value="Incompleto"
												<?php echo valueOf($estado_academico, $paciente->estado_academico, '') == 'Incompleto' ? "selected" : "" ?>>
													Incompleto</option>
										</select></td>
									</tr>
									<tr>
										<td align="left"><span class="letraDisplay" style="padding-left: 12px;">Profesion u Oficio
										</span></td>
										<td><input name="ocupacion" type="text"
											onkeypress="return validFormat(this,event,'Mayus')"
											id="ocupacion" class="letraDisplayInput"
											value="<?php echo valueOf($ocupacion, $paciente->ocupacion, '') ?>" size="55"
											title="Ocupacion" /></td>
									</tr>
								</table>
								<?php } ?>
								<table width="100%" border="0" align="center" cellpadding="2"
									cellspacing="2" class="tablas">
									<tr>
										<td colspan="2" align="left" class="nomGrupo">Informaci&oacute;n
											Asistencial</td>
									</tr>
									<tr>
									  <td  align="left" class="letraDisplay">&nbsp;Persona Con Discapacidad</td>
									  <td><select style="width: 360px;" name="discapacidad" id="discapacidad"  title="Persona Discapacitada" class="letraDisplayInput selectAsistencial">
                                      	<option value="">[SELECCIONE]</option>
                                        <option value="NO" <?php echo valueOf($discapacidad, $paciente->discapacidad, 'NO') == 'NO' ? 'selected' : '' ?>>NO</option>
                                        <option value="SI" <?php echo valueOf($discapacidad, $paciente->discapacidad, 'NO') == 'SI' ? 'selected' : '' ?>>SI</option>
								      </select></td>
								  </tr>
									<tr>
										<td width="240" align="left" class="letraDisplay"><span class="letraRed">*</span>&nbsp;Tipo
											Regimen</td>
										<td width="446"><select  style="width: 360px;" name="regimen"
											class="letraDisplayInput selectAsistencial" id="regimen"
											title="Tipo de Regimen">
												<option>[SELECCIONE]</option>
												<?php $model->elementos('GRL', 'RGMN', 6, valueOf($regimen, $paciente->regimen, '1')); ?>
                                                </select></td>
                                                                        </tr>

									<!-- EPS, ARP Y ARL SOLO APARECEN CUANDO SE ESCOGE ESPAÑOL DE COLOMBIA -->
									<?php if( $_SESSION['language'] == 'es_co' ){ ?>
									<tr>
										<td align="left" class="letraDisplay"><span class="letraRed">*</span>&nbsp;Nombre
											EPS</td>
										<td><input name="cod_eps" readonly type="text"
											class="letraDisplayInput" id="cod_eps"
											value="<?php echo valueOf($cod_eps, $paciente->cod_eps, '') ?>" size="5" /> <input
											name="nombre_eps" type="text" class="cajaBuscar"
											id="nombre_eps" size="44"
											onkeypress="buscarajax(this.value,'nombre|codigo','administradora','tipo = {EPS} AND nombre like ({%'+this.value+'%})','nombre_eps|cod_eps','capa_eps','Nombre Administradora|Codigo',450)"
											value="<?php echo valueOf($nombre_eps, $eps->nombre, ''); ?>"
											title="Administradora de Salud" />

											<div id="capa_eps" style="visibility: visible; z-index: 0;" />
										</td>
									</tr>
									<!-- SOLO SE MUESTRA ARL Y PENSIONES CUANDO LA VERSION ES DIFERENTE A IMAGENES DIAGNOTICAS -->
									<?php if($_SESSION['idversion'] != '3'){ ?>
									
									<!-- ARL y FONDO DE PENSIONES SOLO SE MUESTRAN SI LA VERSION ES DIFERENTE DE CENTRO MEDICO -->
									<?php if($_SESSION['idversion'] != '2'){ ?> 
									<tr>
										<td align="left" class="letraDisplay"><span class="letraRed">*</span> Nombre
											ARL</td>
										<td><input name="codigo_arp" readonly type="text"
											class="letraDisplayInput" id="codigo_arp"
											value="<?php echo valueOf($codigo_arp, $paciente->codigo_arp, '') ?>" size="5" /> <input
											name="nombre_arp" type="text" class="cajaBuscar"
											id="nombre_arp" size="44"
											onkeypress="buscarajax(this.value,'nombre|codigo','administradora','tipo = {ARL} and nombre like ({%'+this.value+'%})','nombre_arp|codigo_arp','capa_arp','Nombre ARL|Codigo',450)"
											value="<?php echo valueOf($nombre_arl, $arp->nombre, ''); ?>"
											title="Administradora de Salud" />

											<div id="capa_arp" style="visibility: visible; z-index: 0;" />
										</td>
									</tr>

									<tr>
										<td align="left" class="letraDisplay" style="padding-left: 12px;" >Fondo Pensiones</td>
										<td><input name="codigo_pensiones" readonly
											type="text" class="letraDisplayInput" id="codigo_pensiones" title="Fondo de Pensiones"
											value="<?php echo valueOf($codigo_pensiones, $paciente->codigo_pensiones, '') ?>" size="5" />
											<input name="nombre_pensiones" type="text" class="cajaBuscar"
											id="nombre_pensiones" size="44"
											onkeypress="buscarajax(this.value,'nombre|codigo','administradora','tipo = {AFP}  and nombre like ({%'+this.value+'%})','nombre_pensiones|codigo_pensiones','capa_pensiones','Nombre PENSIONES|Codigo',450)"
											value="<?php echo valueOf($nombre_pensiones, $pension->nombre, ''); ?>"
											title="Administradora de Salud" />

											<div id="capa_pensiones"
												style="visibility: visible; z-index: 0;" /></td>
									</tr>
									<?php } ?>

									<tr>
										<td align="left" class="letraDisplay"style="padding-left: 12px;"> Prepagada </td>
										<td><input name="codigo_prepagada" readonly
											type="text" class="letraDisplayInput" id="codigo_prepagada"
											value="<?php echo valueOf($codigo_prepagada, $paciente->codigo_prepagada, '') ?>" size="5" />
											<input name="nombre_prepagada" type="text" class="cajaBuscar"
											id="nombre_prepagada" size="44"
											onkeypress="buscarajax(this.value,'nombre|codigo','administradora','tipo = {PREP}  and nombre like ({%'+this.value+'%})','nombre_prepagada|codigo_prepagada','capa_prepagada','Nombre PREPAGADAS|Codigo',450)"
											value="<?php echo valueOf($nombre_prepagada, $prepagada->nombre, ''); ?>"
											title="Administradora Prepagada" />

											<div id="capa_prepagada"
												style="visibility: visible; z-index: 0;" /></td>
									</tr>

									<tr >
										<td align="left" class="letraDisplay"style="padding-left: 12px;"> Caja Compensación </td>
										<td>
										    <input name="cod_caja" readonly v-model="cod_caja"
											type="text" class="letraDisplayInput" id="cod_caja"  
											value="<?php echo valueOf($cod_caja, $paciente->cod_caja, '') ?>" @change="cambiaTipoCaja($event)" size="5" 
											/>
											
											<input name="nombre_caja_compensacion" type="text" class="cajaBuscar" 
											id="nombre_caja_compensacion" size="44" title="Caja Compensación"
											onkeypress="buscarajax(this.value,'nombre|codigo','administradora','tipo = {CCF}  and nombre like ({%'+this.value+'%})','nombre_caja_compensacion|cod_caja','capa_caja','Nombre Caja_Compensacion|Codigo')"
											v-model="nombre_caja_compensacion"
											value="<?php echo valueOf($nombre_caja_compensacion, $caja->nombre, ''); 
													 ?>" 
											title="Administradora Caja Compensacion" />

											<div id="capa_caja"
												style="visibility: visible; z-index: 0;" /></td>
									</tr>
								
								    
										<tr>

										<td  align="left" class="letraDisplay" style="padding-left: 12px;"> Tarifa Caja Compensación</td>
										<td><select style="width: 360px;" name="tarifa_caja" id="tarifa_caja"  title="Tarifa Caja" class="letraDisplayInput selectAsistencial" v-model="tarifa_caja">
												<option value="">[SELECCIONE]</option>
												<option value="A" <?php echo valueOf($tarifa_caja, $paciente->tarifa_caja, 'A') == 'A' ? 'selected' : '' ?>>Tarifa A</option>
												<option value="B" <?php echo valueOf($tarifa_caja, $paciente->tarifa_caja, 'B') == 'B' ? 'selected' : '' ?>>Tarifa B</option>
												<option value="C" <?php echo valueOf($tarifa_caja, $paciente->tarifa_caja, 'C') == 'C' ? 'selected' : '' ?>>Tarifa C</option>
												<option value="D" <?php echo valueOf($tarifa_caja, $paciente->tarifa_caja, 'D') == 'D' ? 'selected' : '' ?>>Tarifa D</option>
										
											</select></td>

										</tr>

									<?php 
									
											}
										} 
									?>
								</table>
							</td>
						</tr>
						  <tr>
						  <td height="19" valign="top">
                          <table width="100%" border="0" cellpadding="2" cellspacing="2" class="tablas">
						    <tr>
						      <td colspan="2" class="nomGrupo"> Contacto de Emergencia  </td>
					        </tr>
						    <tr>
						      <td  align="left" class="letraDisplay"><span class="letraRed">*</span>&nbsp; Nombre
						        Responsable</td>
						      <td width="73%"><input style="margin-left: 30px;" name="nombre_responsable" type="text"
											class="letraDisplayInput" id="nombre_responsable"
											onkeypress="return validFormat(this,event,'Minus')"
											value="<?php echo valueOf($nombre_responsable, $paciente->nombre_responsable, '') ?>"
											size="55" title="Nombre Responsable" /></td>
					        </tr>
							<tr class="tablas" v-if="esMenorEdad">
						      <td align="left" class="letraDisplay"><span class="letraRed">*</span>&nbsp; Identificación</td>
						      <td><input style="margin-left: 30px;" name="iden_responsable" type="text"
											class="letraDisplayInput" id="iden_responsable"
											onkeypress="return validFormat(this,event,'Minus')"
											value="<?php echo valueOf($iden_responsable, $paciente->iden_responsable, '') ?>"
											size="55" title="Identificación Responsable" /></td>
					        </tr>
							<tr class="tablas" v-if="esMenorEdad">
						      <td align="left" class="letraDisplay"><span class="letraRed">*</span>&nbsp; Correo Responsable</td>
						      <td><input style="margin-left: 30px;" name="email_responsable" type="text"
											class="letraDisplayInput" id="email_responsable"
											onkeypress="return validFormat(this,event,'Minus')"
											value="<?php echo valueOf($email_responsable, $paciente->email_responsable, '') ?>"
											size="55" title="Correo Responsable" /></td>
					        </tr>
						    <tr class="tablas" >
						      <td align="left" class="letraDisplay"><span class="letraRed">*</span>&nbsp; Telefono Responsable</td>
						      <td><input style="margin-left: 30px;" name="telefono_responsable" type="text"
											class="letraDisplayInput" id="telefono_responsable"
											onkeypress="return validFormat(this,event,'Minus')"
											value="<?php echo valueOf($telefono_responsable, $paciente->telefono_responsable, '') ?>"
											size="55" title="Telefono Responsable" /></td>
					        </tr>
						    <tr class="tablas" v-if="!esMenorEdad">
						      <td align="left" class="letraDisplay"style="padding-left: 12px;"><span class="letraRed">*</span> Parentesco</td>
						      <td><input style="margin-left: 30px;" name="parentesco_responsable" type="text"
											class="letraDisplayInput" id="parentesco_responsable"
											onkeypress="return validFormat(this,event,'Minus')"
											value="<?php echo valueOf($parentesco_responsable, $paciente->parentesco_responsable, '') ?>"
											size="55" title="Parentesco" /></td>
					        </tr>
							<tr class="tablas" v-else>
						      <td align="left" class="letraDisplay"><span class="letraRed">*</span>&nbsp; Parentesco</td>
						      <td><select style="margin-left: 30px;" name="parentesco_responsable" id="parentesco_responsable"
																    class="letraDisplayInput selectDatosBasicos" title="Tipo de Vivienda">
																<option value="">[Seleccione]</option>
																<?php $model->elementos('GRL', 'PAREN', 5, valueOf($parentesco_responsable,$paciente->parentesco_responsable, ''));?>
								  </select></td>
					        </tr>
							<!-- PAIS CIUDAD BARRIO RESPONSABLE-->
							<tr v-if="esMenorEdad">
									<td align="left" class="letraDisplay">&nbsp;Pais</td>
										<td>
											<select  style="margin-left: 30px;width: 360px;"  name="pais_responsable" title="Pais" class="input" id="pais_responsable" style="width: 80%;"
												ref="pais_responsable" @change="buscarCiudadResponsable($event)">
												<option value="">[SELECCIONE]</option>
												<?php 
													echo $model->crearCombo("paises", "descripcion", "descripcion as des", 
														"idpais IS NOT NULL AND descripcion IS NOT NULL AND idpais='34' ORDER BY des ASC", 
														valueOf($pais_responsable, $paciente->pais_responsable, 'COLOMBIA') );
												?>
											</select>
										</td>
									</tr>
							<tr v-if="esMenorEdad">
									<td align="left" class="letraDisplay">&nbsp;<?php echo $i18n->translate('ciudad'); ?></td>
										<td>
										<input type="hidden" name="muni_responsable" id="muni_responsable">
											<input type="hidden" name="ciudad_responsable" id="ciudad_responsable" ref="ciudad_responsable" 
												value="<?php echo valueOf($ciudad_responsable, '') ?>">
											
												
											
											<?php 
												 $nombrePaisResponsable = valueOf($pais_responsable, $paciente->pais_responsable, 'COLOMBIA');
												 $codigo_pais_responsable = $model->getDato('idpais', 'paises', "descripcion = '{$nombrePaisResponsable}'");
											 ?>
											<select style="margin-left: 30px;width: 360px;"  name="cod_muni_responsable" title="Ciudad Responsable" class="input" id="cod_muni_responsable" ref="cod_muni_responsable">
 												<option value="" selected>[SELECCIONE]</option>
												<?php 
													echo $model->crearComboCiudad("sis_muni m,departamentos d","m.codigo","CONCAT(m.nombre,' - (',d.nombre,')')", 
														"m.id_dep=d.codigo AND m.codigo is not null and m.nombre is not null and 
														m.idpais = '". $codigo_pais ."' order by m.nombre asc", 
														valueOf($cod_muni_responsable, $paciente->cod_muni_responsable, '') ) ;
												?>
											</select>
										</td>
									</tr>
									<tr  v-if="esMenorEdad">
										<td align="left" class="letraDisplay"><span class="letraRed">*</span>&nbsp;<?php echo $i18n->translate('barrio'); ?></td>
										<td>
											<input type="hidden" name="barriohdd" id="barriohdd">
											 <input name="barrio_responsable" type="hidden" class="letraDisplayInput" id="barrio_responsable" 
	 											value="<?php echo valueOf($barrio_responsable, $paciente->barrio_responsable, '') ?>" size="6"/>
                                            <input style="margin-left: 30px;"  name="nombre_barrio_responsable"	title="Barrio Responsable" type="text" class="cajaBuscar" 
                                            		id="nombre_barrio_responsable" ref="nombre_barrio_responsable"
		                                            @keyup="_.delay( () => filtarBarriosResponsable($event), 150)"
		                                            value="<?php echo utf8_decode( $model->getDato('nombres', 'sis_barrios', "codigo = '". valueOf($barrio_responsable, $paciente->barrio_responsable, '') ."'") ); ?>" size="55" />
											<input type="hidden" name="cod_dep_responsable" id="cod_dep_responsable" value="<?php echo  !empty($paciente->cod_dep_responsable) ? $paciente->cod_dep_responsable: $model->getDato('id_dep', 'sis_muni', "codigo = '{$cod_muni_responsable}'")  ?>" />
											<div id="capa_barrio_responsable" style="visibility: visible; z-index: 0; margin-left: 30px;" />
										</td>
									</tr>
							<!-- FIN PAIS CIUDAD BARRIO RESPONSABLE -->
					      </table></td>
					  	</tr>
						<tr>
							<td height="75" valign="top">
								<table width="100%" border="0" cellpadding="2" cellspacing="1"
									>
									<tr>
										<td colspan="2" class="nomGrupo">Informaci&oacute;n del Registro</td>
									</tr>
									<tr>
									  <td align="left" class="letraDisplay"style="padding-left: 12px;" > Referencia de Solicitud</td>
									  <td><input name="referencia" type="text" title="Numero de Solicitud"
											class="letraDisplayInput" id="referencia"
											value="<?php echo empty($_SESSION['solicitud']) ? "" : $_SESSION['solicitud']; ?>"
											size="55" readonly /></td>
								  </tr>
									<tr>
										<td width="240" align="left" class="letraDisplay" style="padding-left: 12px;">Usuario
											Creador</td>
									  <td width="446"><input name="nomcreador" type="text"
											title="Usuario Creador" class="letraDisplayInput"
											id="nomcreador" size="55" readonly
											value="<?php echo valueOf($nomcreador, $paciente->nomcreador, $_SESSION['user']) ?>" /></td>
									</tr>
									<tr>
										<td align="left" class="letraDisplay"style="padding-left: 12px;" >Fecha Creacion</td>
										<td><input name="creacion" type="text"
											class="letraDisplayInput" id="creacion"
											value="<?php echo valueOf($creacion, $paciente->creacion, date('Y-m-d H:i:s')) ?>"
											size="55" readonly /></td>
									</tr>
									<tr>
									<td align="left" valign="top" class="letraDisplay">&nbsp;  Observaciones</td>
										<td><span class="letraDisplay"> <textarea name="observacion"
													cols="57" rows="4" class="letraDisplayInput"
													id="observacion" title="Observacion"><?php echo valueOf($observacion, $paciente->observacion, '') ?></textarea>
										</span></td>
									</tr>
								</table>
						  </td>
						</tr>
						<tr>
							<td height="16" valign="top">
                            	<input type="hidden" name="codcreador" id="codcreador" value="<?php echo valueOf($codcreador, $paciente->codcreador, $_SESSION['codigo_user']) ?>" />
                            </td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td valign="middle" scope="col">
					<table border="0" align="center" cellpadding="9" cellspacing="0">
						<tr>
							<td width="545" height="20" valign="middle" align="center">

							<input type="button" class="boton" onClick="javascript:ejecutar(this.value)" value="Guardar">

                            </td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</form>

<?php $wmodel->Pie(); ?>
<!--<object ID="GrFingerX"
		CLASSID="CLSID:71944DD6-B5D2-4558-AD02-0435CB2B39DF"></object>-->

<script language="JavaScript"	src="<?php echo $_SESSION['path']; ?>/librerias/biometria/cliente/GrFinger.js"></script>
<script type="text/javascript">var i = 0;</script>
<script for="GrFingerX" event="SensorPlug(id)" language="javascript">
    GrFingerX.CapStartCapture(id);
</script>

<SCRIPT FOR="GrFingerX" EVENT="ImageAcquired(id, w, h, rawImg, res)"
	LANGUAGE="javascript">
    GrFingerX.CapSaveRawImageToFile(rawImg, w, h, "C:\\" + document.getElementById('num_id').value + ".bmp", 501);
    var result = GrFingerX.ExtractJSON(rawImg, w, h, res, GrFingerX.GR_DEFAULT_CONTEXT, GrFingerX.GR_FORMAT_DEFAULT);
    var Objret = eval('(' + result + ')');	//Adaptate the return to an Object form
    document.getElementById("rtemplate").value = result;
    Start(Objret.ret);

    if (document.getElementById('img').style.display == 'none')
        document.getElementById('img').style.display = 'block';
    //CallEnroll(rawImg, w, h, res);
</SCRIPT>

<script>

		function ejecutar(orden){

			switch(orden){

				case 'Guardar':
					Enviar('formulario','Basicos.php?operacion=guardar',
					'observacion|num_seguridad_social|discapacidad|tipo_sangre|codigo_prepagada|nombre_prepagada|referencia|huella|fuente|firmapaciente|rtemplate|cod_muni|nombre_pais|usuario_twitter|nombre_arp|usuario_skype|usuario_facebook|segundo_nom|ocupacion|cargo|telefono|tel_oficina|ciudadoficina|acompanante|tel_celular|tacomp2|departamento|ciudad|barrio|oficina|foto|comisionado|remisor|nombre_comisionado|numero_hijo|estado_academico|codigo_pensiones|nombre_pensiones|cod_caja|nombre_caja_compensacion|tarifa_caja|');
				break;
			}
		}

</script>


<script>
    function buscarPaciente() {
        Enviar('formulario', 'Basicos.php?operacion=buscar_paciente', '');
    }

    function Imprimir() {
        ImprimirReporte('codrpt=003&id=' + document.formulario.id_muni.value);
    }
	
	function colonias(colonias){
		var muni = document.formulario.cod_muni_responsable.value;
		buscarajax(colonias,'b.nombres, m.nombre, d.nombre, b.municipio, b.zona, b.codigo, p.descripcion','sis_barrios b, sis_muni m, departamentos d, paises p', 'b.nombres like ({%'+colonias+'%}) and b.municipio = m.codigo and d.codigo = b.dept and m.idpais = p.idpais and m.codigo='+muni+'' ,'nombre_barrio|nombre_ciudad|dept|cod_dep|zona|barrio', 'capa_barrio','Barrio|Municipio|Departamento','520|320|300')
	}
	// (function(){
	// 	buscarCiudad($("#nombre_pais")[0]);
	// 	buscarBarrios({ value: '<?php echo $paciente->cod_muni ?>' });
	// })();
</script> 

<script>
	new Vue({
		el: '#formulario',
		data:{
			annio : '',
			mes   : '',
			dia   : '',
			edadPaciente  : '',
			tipo_id: '',
			numId: '',
			primer_nom  : '',
			segundo_nom : '',
			primer_ape  : '',
			segundo_ape : '',
			tarifa_caja : '',
			sexo: '',
			cod_caja:'',
			nombre_caja_compensacion:'',   
		},
		created () {
			//let self = this;
			this.annio 	= document.querySelector("select[name=xnho]").value;
			this.mes   	= document.querySelector("select[name=xmes]").value;
			this.dia   	= document.querySelector("select[name=xdia]").value;
			
			this.tipo_id = document.getElementById("tipo_id").value;
			this.numId  = document.getElementById("num_id").value;
			this.primer_nom  = document.getElementById("primer_nom").value;
			this.segundo_nom = document.getElementById("segundo_nom").value;
			this.primer_ape  = document.getElementById("primer_ape").value;
			this.segundo_ape = document.getElementById("segundo_ape").value;
			this.tarifa_caja = document.getElementById("tarifa_caja").value;
			this.sexo = document.getElementById("sexo").value;
			this.cod_caja = document.getElementById("cod_caja").value;
			this.nombre_caja_compensacion  = document.getElementById("nombre_caja_compensacion").value;
		},
		methods: {
			filtarBarrios(e){
				console.log(e.target.value+"-"+this.$refs.cod_muni.value)
				buscarajax(e.target.value,'b.nombres,m.nombre,d.nombre,b.codigo,b.dept','sis_barrios b,sis_muni m, departamentos d', 
					"b.municipio=m.codigo AND b.dept=d.codigo AND nombres like ('%" + e.target.value + "%') and b.municipio = '" + this.$refs.cod_muni.value + "'",
					'nombre_barrio|nombre_municipio|nombre_departamento|barrio|cod_dep', 'capa_barrio','Barrio|Municipio|Departamento','440|280|280');
/* 				let municipioText = this.$refs.cod_muni.options[this.$refs.cod_muni.selectedIndex].text;
				buscarajax(e.target.value,'b.nombres,m.nombre,d.nombre,b.codigo,b.dept','sis_barrios b,sis_muni m, departamentos d', 
					"b.municipio=m.codigo AND b.dept=d.codigo AND nombres like ('%" + e.target.value + "%') and m.nombre  like '%" + municipioText + "%'",
					'nombre_barrio|nombre_municipio|nombre_departamento|barrio|cod_dep', 'capa_barrio','Barrio|Municipio|Departamento','440|280|280'); */
			},
			filtarBarriosResponsable(e){
				console.log("responsable"+e.target.value+"-"+this.$refs.cod_muni_responsable.value)
				buscarajax(e.target.value,'b.nombres,m.nombre,d.nombre,b.codigo,b.dept','sis_barrios b,sis_muni m, departamentos d', 
					"b.municipio=m.codigo AND b.dept=d.codigo  AND nombres like ('%" + e.target.value + "%') and municipio = '" + this.$refs.cod_muni_responsable.value + "'",
					'nombre_barrio_responsable|nombre_municipio_responsable|nombre_departamento_responsable|barrio_responsable|cod_dep_responsable', 'capa_barrio_responsable','Barrio|Municipio|Departamento','300|200|200');
				
/* 				let municipioResponsable = this.$refs.cod_muni_responsable.options[this.$refs.cod_muni_responsable.selectedIndex].text;
				buscarajax(e.target.value,'b.nombres,m.nombre,d.nombre,b.codigo,b.dept','sis_barrios b,sis_muni m, departamentos d', 
					"b.municipio=m.codigo AND b.dept=d.codigo AND nombres like ('%" + e.target.value + "%') and m.nombre  like '%" + municipioResponsable + "%'",
					'nombre_barrio_responsable|nombre_municipio_responsable|nombre_departamento_responsable|barrio_responsable|cod_dep_responsable', 'capa_barrio_responsable','Barrio|Municipio|Departamento','300|200|200'); */
				
			},
			checkAnnio(){
			 	this.annio = document.querySelector("select[name=xnho]").value;
			},
			checkMes(){
			 	this.mes   = document.querySelector("select[name=xmes]").value;
			},
			checkDia(){
			 	this.dia   = document.querySelector("select[name=xdia]").value;
			},
			checkNumId(){
				this.numId  = document.getElementById("num_id").value;
			},
			checkTipoId(){
				this.tipo_id = document.getElementById("tipo_id").value;
			},
			buscarCiudad(event){
			const c = event.target;
			$("#cod_muni").html("");
			$("#cod_muni").append("<option>Cargando..</option>");
				return $.ajax({
					type: 'POST',
					url: 'CtrlBasicos.php',
					data: {operacion: 'BuscarCiudad', idpais: c.value},
					success: function(ciudades){
						//console.log("Ciudades + "+ciudades);
						var selectCiudades = $('#cod_muni');
						selectCiudades.html(ciudades);
						var muni = document.querySelector('#muni').value;
						if(muni){
							$("#cod_muni").val(muni);
						}else{
							console.log("<?php echo $paciente->cod_muni_responsable ?>");
							var ciudad_paciente = "<?php echo $paciente->cod_muni ?>";
							$("#cod_muni").val(ciudad_paciente);
						}	
					},error:function(error){console.log(error)}
					
				});
			},
			buscarCiudadResponsable(event){
			const c = event.target;
			$("#cod_muni_responsable").html("");
			$("#cod_muni_responsable").append("<option>Cargando..</option>");
			return $.ajax({
				type: 'POST',
				url: 'CtrlBasicos.php',
				data: {operacion: 'BuscarCiudad', idpais: c.value},
				success: function(ciudades){
					console.log("Ciudades + "+ciudades);
					var selectCiudades = $('#cod_muni_responsable');
					selectCiudades.html(ciudades);
					var muni = document.querySelector('#muni_responsable').value;
					if(muni){
						$("#cod_muni_responsable").val(muni);
					}else{
						console.log("<?php echo $paciente->cod_muni_responsable ?>");
						var ciudad_paciente = "<?php echo $paciente->cod_muni ?>";
						$("#cod_muni_responsable").val(ciudad_paciente);
					}
				},error:function(error){console.log(error)}

			});
		},
		verificarDerechos(){
			let loader = `<img src="<?php echo $_SESSION['path']; ?>/imagenes/loading.gif" border="0" height="15px"/>`
			document.getElementById('verificador').innerHTML = loader;
			if(!this.numId){
				new PNotify({title: 'Informe de transación',text: 'Por favor Ingrese Número de indentificación',type:'error',delay: 900});
				document.getElementById('verificador').innerHTML = 'Verificar';
				return;
			}
			fetch('<?php echo $_SESSION['isw_api']?>/verificador?tipo_id='+this.tipo_id+'&num_id='+this.numId, { mode: 'cors' })
			.then( res => res.text() )
			.then( respuesta => {
				return new DOMParser().parseFromString(respuesta, 'text/xml');	
			})
			.then( xml => {
				let datosVerificados = false;
				let titular;
				if(this.numId == this.getPropiedad(xml, 'TitularNumeroDocumento')){
					console.log("Titular");
					this.cargarDatosXML(xml,true);
					datosVerificados = true;
					titular = true;
				}else{
					console.log("Beneficiario");
					let listPacs = xml.querySelectorAll('pacs');
					let numIdIngresado = this.numId;

					listPacs.forEach( (pacs,i) => {
						let numidBeneficiario = pacs.querySelector('PACNumeroDocumento').textContent;
						if(numIdIngresado == numidBeneficiario){
							this.cargarDatosXML(pacs,false);
							datosVerificados = true;
						}
					});
				}
				if ( !datosVerificados ){
					new PNotify({title: 'Paciente No Encontrado',text: 'Por favor Intente Nuevamente',delay: 900});
				}else{
					// this.tarifa_caja = this.getPropiedad(xml, 'Tarifa');
					// Si la tarifa es A,B,C trae caja compensación COMFAMA
					if(this.tarifa_caja != 'D'){
						this.cod_caja = 'CCF04';
						this.getNombreCaja(this.cod_caja);
					}
					new PNotify({title: 'Informe Transacción',text: titular ? 'Paciente Verificado Como Titular' : 'Paciente Verificado Como Beneficiario' ,type:'success',delay: 2000});
				}
				document.getElementById('verificador').innerHTML="Verificar";
			})
			.catch(function(err) {
				alert("Error: " + err );
    			new PNotify({title: 'Informe Transacción',text: 'Error al Verificar',type:'error',delay: 900});
				document.getElementById('verificador').innerHTML="Verificar";
  			});
		},
		getPropiedad(xml, propiedad) { 
			return xml.querySelector(propiedad).textContent; 
		},
		getNombreCaja(cod_caja){
			fetch('CtrlBasicos.php?operacion=buscarCaja&cod_caja='+cod_caja, { mode: 'cors' })
			.then(res => res.text()) 
  			.then(respuesta => { this.nombre_caja_compensacion = respuesta;})
			.catch(function(err) {
				alert("Error: al Cargar caja" + err );
  			});
		},
		cargarDatosXML(xml,titular){
			this.fecha_nacimiento  = titular ? this.getPropiedad(xml, 'TitularFechaNacimiento') : this.getPropiedad(xml, 'PACFechaNacimiento') ;
			this.primer_nom        = titular ? this.getPropiedad(xml, 'TitularPrimerNombre')    : this.getPropiedad(xml, 'PACPrimerNombre') ;
			this.segundo_nom       = titular ? this.getPropiedad(xml, 'TitularSegundoNombre')   : this.getPropiedad(xml, 'PACSegundoNombre') ;
			this.primer_ape        = titular ? this.getPropiedad(xml, 'TitularPrimerApellido')  : this.getPropiedad(xml, 'PACPrimerApellido') ;
			this.segundo_ape       = titular ? this.getPropiedad(xml, 'TitularSegundoApellido') : this.getPropiedad(xml, 'PACSegundoApellido') ;
			this.numId             = titular ? this.getPropiedad(xml, 'TitularNumeroDocumento') : this.getPropiedad(xml, 'PACNumeroDocumento') ;
			this.sexo              = titular ? this.getPropiedad(xml, 'TitularGenero') 	    : this.getPropiedad(xml, 'PACGenero') ;
			this.tarifa_caja       = titular ? this.getPropiedad(xml, 'Tarifa')                 : this.getPropiedad(xml, 'PACCategoriaSalarial') ;
			this.sexo              = (this.sexo == 0) ?  'M' : 'F'; 
			var fecha_nacimiento   = titular ? this.getPropiedad(xml, 'TitularFechaNacimiento') : this.getPropiedad(xml, 'PACFechaNacimiento') ;
			if(this.fecha_nacimiento){
				this.annio 	= fecha_nacimiento.substr(0, 4);
				this.mes   	= fecha_nacimiento.substr(-4, 2);
				this.dia   	= fecha_nacimiento.substr(-2);
			}
			if (!this.tarifa_caja){
				this.tarifa_caja = 'D';
			}
			
		}
		},
		computed:{
			edad: function(){
				const convertAge = new Date(this.annio,this.mes,this.dia);
				const timeDiff = Math.abs(Date.now() - convertAge.getTime());
				showAge = Math.floor((timeDiff / (1000 * 3600 * 24))/365);
				this.edadPaciente = showAge;
				return showAge;
			},
			esMenorEdad: function(){
				return this.edad < 18 ? true : false ;
			},
		}
	});
</script>
</body>
</html>
